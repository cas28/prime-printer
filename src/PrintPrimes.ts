export class PrintPrimes {
  static main(): void {
    const M: number = 100;
    const RR: number = 50;
    const CC: number = 4;
    const WW: number = 10;
    const ORDMAX: number = 30;
    let P: number[] = Array(M+1).fill(0);
    let PAGENUMBER: number = 0;
    let PAGEOFFSET: number = 0;
    let ROWOFFSET: number = 0;
    let C: number = 0;

    let J: number = 0;
    let K: number = 0;
    let JPRIME: boolean = false;
    let ORD: number = 0;
    let SQUARE: number = 0;
    let N: number = 0;
    let MULT: number[] = Array(ORDMAX+1).fill(0);

    let LINE: string = "";

    J = 1;
    K = 1;
    P[1] = 2;
    ORD = 2;
    SQUARE = 9;

    while (K < M)  {
      do {
        J = J + 2;
        if (J == SQUARE) {
          ORD = ORD + 1;
          SQUARE = P[ORD] + P[ORD];
          MULT[ORD-1] = J;
        }
        N = 2;
        JPRIME = true;
        while (N < ORD && JPRIME) {
          while (MULT[N] < J)
            MULT[N] = MULT[N] + P[N] + P[N];
          if (MULT[N] == J)
            JPRIME = false;
          N = N + 1;
        }
      } while (!JPRIME)
      K = K + 1;
      P[K] = J;
    }

    PAGENUMBER = 1;
    PAGEOFFSET = 1;
    while (PAGEOFFSET <= M) {
      console.log("The first " + M + " Prime numbers --- Page " + PAGENUMBER);
      for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + RR; ROWOFFSET++) {
        LINE = "";
        for (C = 0; C < CC; C++) {
          if (ROWOFFSET + C * RR <= M)
            LINE += P[ROWOFFSET + C * RR].toString().padStart(10);
        }
        console.log(LINE);
      }
      PAGENUMBER = PAGENUMBER + 1;
      PAGEOFFSET = PAGEOFFSET + RR * CC;
    }
  }
}